document.addEventListener('DOMContentLoaded', () => {

    let api = 'https://kitsu.io/api/edge/anime'
    let views = ['#list-main', '#single-main', '#pagination']
    let heroesList = document.querySelector('#list-heroes')
    let heroeInfo = document.querySelector('#hero-details')
    let backButton = document.querySelector('#back-hero-list')
    let input = document.querySelector('#filter-input')
    let table = document.querySelector('#filter-table')
    let paginationHTML = document.querySelector('#pagination')

    init = () => {
        getAllHeroes(0);

        backButton.onclick = () => {
            getAllHeroes(+window.localStorage.getItem('page'))
        }
    }


    getAllHeroes = (page) => {
        window.localStorage.setItem('page', page)
        renderList(service(null, (page * 10)))
    }

    heroDetails = (id) => {
        renderHero(service(id))
    }

    selectHeroClick = (action) => {
        let elementTarget = document.querySelectorAll('.hero-list')
        elementTarget.forEach(hero => {
            if (action === 'add') {
                hero.addEventListener('click', (evt) => {
                    heroDetails(evt.target.closest('tr').dataset.hero)
                })
            } else {
                hero.removeEventListener('click', () => { })
            }
        })
    }

    changeView = () => {
        views.filter(view => {
            const el = document.querySelector(`${view}`);
            el.classList.contains('hide') ? el.classList.remove('hide') : el.classList.add('hide')
        })
    }

    service = (id, qty) => {
        const type = {
            null: () => fetch(`${api}?page[limit]=10&page[offset]=${qty}`).then(res => res.json()).then(data => data),
            'default': () => fetch(`${api}/${id}`).then(res => res.json()).then(data => data)
        }
        return (type[id] || type['default'])()
    }

    renderList = (data) => {
        changeView()
        selectHeroClick('remove')
        setOverlay(true)
        heroesList.innerHTML = ''
        data.then(result => {
            pagination(+window.localStorage.getItem('page'), result.meta.count)
            result.data.forEach(hero => {
                heroesList.innerHTML += createDOMElement(hero, 'list')
            })
            setOverlay(false)
            selectHeroClick('add')
        })
    }

    renderHero = (data) => {
        changeView()
        selectHeroClick('remove')
        setOverlay(true)
        heroeInfo.innerHTML = ''
        data.then(result => {
            console.log(result);
            heroeInfo.innerHTML = createDOMElement(result.data, 'single')
            setOverlay(false)
        })
    }

    createDOMElement = (hero, type) => {
        if (type === 'list') {
            let template = `
                <tr class="hero-list" data-hero="${hero.id}">
                    <td class="flex flex-direction-row align-center">
                        <img src="${hero.attributes.posterImage.original}" alt="${hero.attributes.titles.en_jp}" class="profile-pic">
                        <h4 class="hero-name">${hero.attributes.titles.en_jp}</h4>
                    </td>
                    <td class="hide-mobile">${hero.attributes.synopsis}</td class="hide-mobile">
                </tr>
                `
            return template
        } else {
            let template = `
                <div class="hero-details-header">
                    <img src="${hero.attributes.posterImage.original}" alt="${hero.attributes.titles.en}">
                    <h1 class="hero-details-name">${hero.attributes.titles.en} (${hero.attributes.titles.ja_jp})</h1>
                    <div class="flex flex-direction-row justify-between align-center hero-info hide-mobile">
                    <span class="hero-details-classificacao">Classificação: <span>${hero.attributes.ratingRank}</span></span>
                    <span class="hero-details-classificacao">Classificação média: <span>${hero.attributes.averageRating}</span></span>
                    <span class="hero-details-classificacao">Ranking de popularidade: <span>${hero.attributes.popularityRank}</span></span>
                    </div>
                    <h6 class="hero-details-desc">${hero.attributes.synopsis}</h6>
                </div>
                <div class="video-container">
                    <iframe src="https://www.youtube.com/embed/${hero.attributes.youtubeVideoId}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                `
            return template;
        }
    }

    setOverlay = (bool) => {
        let overlay = document.querySelector('.overlay');
        (bool) ? overlay.style.display = 'block' : overlay.style.display = 'none'
    }

    filter = (val) => {
        if (val.length) {
            paginationHTML.style.display = 'none'
            fetch(`${api}?filter[text]=${val.toLowerCase()}`).then(res => res.json()).then(result => {
                heroesList.innerHTML = ''
                result.data.forEach(hero => {
                    heroesList.innerHTML += createDOMElement(hero, 'list')
                })
            })
        } else {
            heroesList.innerHTML = ''
            paginationHTML.style.display = ''
            getAllHeroes(+window.localStorage.getItem('page'))
            changeView()
        }

        // let filter = input.value.toUpperCase()
        // let tr = table.getElementsByTagName('tr')

        // for (let i = 0; i < tr.length; i++) {
        //     td = tr[i].getElementsByTagName('td')[0]
        //     if (td) {
        //         txtValue = td.textContent || td.innerText
        //         if (txtValue.toUpperCase().indexOf(filter) > -1) {
        //             tr[i].style.display = ''
        //         } else {
        //             tr[i].style.display = 'none'
        //         }
        //     }
        // }
    }

    pagination = (page, totalItems) => {

        paginationHTML.querySelectorAll('li').forEach(page => {
            page.removeEventListener('click', () => { })
        })
        paginationHTML.innerHTML = ''
        let totalPages = Math.ceil(totalItems / 10)
        let currentPage = page + 1
        let pageSize = 10
        let maxPages = 3

        if (currentPage < 1) {
            currentPage = 1
        } else if (currentPage > totalPages) {
            currentPage = totalPages
        }

        let startPage, endPage

        if (totalPages <= maxPages) {
            startPage = 1;
            endPage = totalPages;
        } else {
            let maxPagesBeforeCurrentPage = Math.floor(maxPages / 2);
            let maxPagesAfterCurrentPage = Math.ceil(maxPages / 2) - 1;
            if (currentPage <= maxPagesBeforeCurrentPage) {
                startPage = 1;
                endPage = maxPages;
            } else if (currentPage + maxPagesAfterCurrentPage >= totalPages) {
                startPage = totalPages - maxPages + 1;
                endPage = totalPages;
            } else {
                startPage = currentPage - maxPagesBeforeCurrentPage;
                endPage = currentPage + maxPagesAfterCurrentPage;
            }
        }

        let pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

        paginationHTML.innerHTML = `${currentPage === 1 ? `<li class="disable-click" data-page="${pages[0]}"></li>` : `<li data-page="1" data-page="${pages[0]}"></li>`}
			${currentPage === 1 ? `<li><a data-page="${pages[0]}" class="active">${pages[0]}</a></li>` : `<li><a data-page="${pages[0]}">${pages[0]}</a></li>`}
            ${currentPage !== 1 && currentPage != totalPages ? `<li><a data-page="${pages[1]}" class="active">${pages[1]}</a></li>` : `<li><a data-page="${pages[1]}">${pages[1]}</a></li>`}\
            ${currentPage === totalPages ? `<li><a data-page="${pages[2]}" class="active">${pages[2]}</a></li>` : `<li><a data-page="${pages[2]}">${pages[2]}</a></li>`}           
			${currentPage === totalPages ? `<li class="disable-click" data-page="${totalPages}"></li>` : `<li data-page="${totalPages}"></li>`}
			`;

        paginationHTML.querySelectorAll('li').forEach(page => {
            page.addEventListener('click', (evt) => {
                getAllHeroes((evt.target.dataset.page) - 1)
                changeView()
            })
        })
    }

    // init
    init();


});